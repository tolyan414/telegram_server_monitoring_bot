exports.commands = {
	'/uptime' : 'uptime',
	'/df'     : 'df -h |grep "/$"',
	'/pm2'    : 'pm2 status -m',
	'/phprestart'   : '/etc/init.d/php5-fpm restart',
	'/mysqlrestart' : '/etc/init.d/php5-fpm restart',
	'/lighttpdrestart' : '/etc/init.d/lighttpd restart',

	'/help'   : 'echo "/uptime    /df    /pm2    /lighttpdstatus    /phprestart    /mysqlrestart    /lighttpdrestart    /help"'
}