var Config = require("./config").config;
var commands = require("./commands.js").commands;
var exec = require('child_process').exec;

var TelegramBot = require('node-telegram-bot-api');

var bot = new TelegramBot(Config.token, { polling: true });

bot.onText(/\/lighttpdstatus(.*)/, function (msg, match) {
	if (msg.from.id != Config.allowedChatId) return;

	exec('curl '+Config.ltpdStatusUrl, function(error, stdout, stderr) {
		var res = /<body>([\s\S]*?)<\/table>/.exec(stdout),
		    str = res[0].replace(/<.*?>/g, " ")
		    	.replace(/absolute/, "\nabsolute")
		    	.replace(/sliding average/, "sliding")
		    	.replace(/average/g, "\naverage")
	  bot.sendMessage(msg.chat.id, str || 'empty');  
	});
});

bot.on('message', function (msg) {
  if (msg.from.id != Config.allowedChatId) return;

	var cmd = commands[msg.text];
	if (cmd) {
		exec(cmd, function(error, stdout, stderr) {
		  bot.sendMessage(msg.chat.id, stdout || stderr || 'empty');  
		});
	}
});