## Telegram bot for execute commands on the server

To use a bot on the server must be installed *node js* 

### Installation

Clone a repository to the server:

     git clone https://tolyan414@bitbucket.org/tolyan414/telegram_server_monitoring_bot.git serverMonitoring

Install modules JS site:

     cd serverMonitoring
     npm install

Write to file *config.js* bot token and telegram user ID:

     cp config-template.js config.js
     nano config.js

Start the bot:

     nodejs sr414.js

Or do it via *node js Process Manager*

     pm2 start sr414.js

Start your telegram messenger and send */help* command to bot


# Телеграм бот для выполнения команд на сервере

Для работы бота на сервере должен быть установлен *node js*

## Установка

Клонируйте репозиторий на сервер:

     git clone https://tolyan414@bitbucket.org/tolyan414/telegram_server_monitoring_bot.git serverMonitoring

Установите модули node js:

     cd serverMonitoring
     npm install

Напишите в файл *config.js* токен бота и telegram user id:

     cp config-template.js config.js
     nano config.js

Запустите бот:

     nodejs sr414.js

Или сделайте это с помощью *node js process manager*

     pm2 start sr414.js

Дайте команду */help* боту из мессенджера телеграм